use std::process::Command;
use crate::ProcessInfo;
use crate::{Error, ErrorKind, Result};

pub fn list_processes() -> Result<Vec<ProcessInfo>> {
    let psout = Command::new("ps")
        .arg("x")
        .output()?;
    if !psout.status.success() {
        return Err(ErrorKind::ProcessListFailed.into());
    }
    let out = String::from_utf8(psout.stdout)?;
    let mut lines = out.as_str().lines();

    let mut processes = Vec::new();
    let header = lines.next()
        .ok_or(Error::from(ErrorKind::ProcessListFailed))?;

    let end_pid = header.find("PID")
        .ok_or(Error::from(ErrorKind::ProcessListFailed))? + 3;
    
    let start_cmd = header.find("COMMAND")
        .ok_or(Error::from(ErrorKind::ProcessListFailed))?;
    
    for ln in lines {
        let pid = ln[0..end_pid].trim_start().parse::<i32>()?;
        let cmd = &ln[start_cmd..];
        processes.push(ProcessInfo {
            command: cmd.into(),
            pid,
        });
    }

    Ok(processes)
}

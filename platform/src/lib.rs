#![recursion_limit = "1024"]

#[macro_use]
extern crate error_chain;

#[cfg(any(target_os = "linux", target_os = "macos"))]
mod ps;
#[cfg(any(target_os = "linux", target_os = "macos"))]
pub use ps::list_processes;

mod errors;

pub use errors::{Error, ErrorKind, Result};

pub struct ProcessInfo {
    pub command: String,
    pub pid: i32,
}

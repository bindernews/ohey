
use std::cmp;
use std::cmp::PartialEq;
use std::error::Error;
use std::fmt;
use std::rc::Rc;
use std::iter::Iterator;

pub type Offset = u64;

#[derive(Debug)]
pub struct IndexError {
    what: &'static str,
    length: u64,
    index: u64,
}

impl fmt::Display for IndexError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Tried to index into a {} of length {} at index {}",
                self.what, self.length, self.index)
    }
}
impl Error for IndexError {}

// The maximum generation a node can be before wrapping around
const MAX_GENERATION: u16 = 0xFFFE;
const LEFT: bool = true;
const RIGHT: bool = false;

/// A callback which splits the user data T into two new pieces of data at the given offset.
/// The first returned value will represent data from [0..offset) and the second will
/// represent data from [offset..length).
/// 
/// @param data 
/// @param offset the offset of data relative to the start of T
pub type SplitCallback<T> = fn(data: &T, offset: Offset) -> (T, T);

/*
 * Note: The use of WAVL-trees
 * Another option was a Splay-tree however splay-trees aren't thread-safe and mutate on find
 * operations, which isn't great for Rust. Furthermore "cloning" an AVL-tree is as simple as
 * cloning the nodes which lead to the node you want to change, which won't work on Splay-trees.
 */

/// The rope data structure allows find, insert, and remove at random locations in O(log n).
/// It's implemented using an WAVL-tree to prioritize find operations, with the constraint
/// that during rotations all leaf nodes remain ordered.
/// 
/// When cloning, only the modified nodes are changed, reducing memory usage while allowing
/// undo and redo. This is done by tracking the "generation" of both the main rope, and
/// each node.
pub struct Rope<T> {
    root: RefNode<T>,
    on_split: SplitCallback<T>,
    generation: u16,
    invalid_data: T,
}

impl<T: PartialEq> Rope<T> {
    pub fn new(on_split: SplitCallback<T>, invalid_data: T) -> Rope<T> {
        let root = RopeNode::new_ref(0, 0, invalid_data);
        Rope::<T> { root, on_split, generation: 0, invalid_data }
    }

    pub fn len(&self) -> Offset {
        self.root.length
    }

    pub fn index(&self, offset: Offset) -> (T, Offset) {
        let (leaf, local_offset, _path) = self.leaf_at_index(offset);
        return (leaf.data, local_offset);
    }

    pub fn iter(&self, offset: Offset, length: u64) -> RopeIter<T> {

    }

    /// Splits the leaf node at the given offset into two nodes, if necessary.
    pub fn split(&mut self, offset: Offset) {
        let (leaf_orig, local_offset, path) = self.leaf_at_index(offset);
        // If local_offset == 0 then we don't need to split, it's already split there
        if local_offset == 0 {
            return;
        }

        // Clone any nodes we may be modifying
        self.clone_path(&path, 0);
        // And perform the split!
        self.danger_split(&path, offset, local_offset);
    }

    /// Insert new data into the rope at the specified position.
    pub fn insert(&mut self, offset: Offset, length: Offset, data: T) {
        // There are no gaps in the rope, so at most we can append the data
        if offset > self.len() {
            panic!("Can not insert data past the length of the Rope");
        }
        // Rope is empty, insert at the beginning, also (&& offset == 0) is implied here
        // because if offset != 0, then offset > self.len() would be true
        if self.len() == 0 {
            self.root.left = Some(RopeNode::new_ref(length, self.generation, data));
            self.root.length = length;
        }
        // Standard insert, find the location to insert at, split if necessary, and 
        // 
        else {
            let (leaf, local_offset, path) = self.leaf_at_index(offset);
            unimplemented!()
        }
    }

    /// Ensure that we're not modifying anyone else's copy of the nodes from the
    /// root down following the path, this is how we achieve immutability.
    /// Returns the node found by traversing through result_index nodes, to save
    /// an extra traversal later. This means result_index of 0 gives the node under root.
    fn clone_path(&mut self, path: &[bool], result_index: usize) -> Option<RefNode<T>> {
        // Starting from the top, only clone nodes if they're not of our generation
        let mut result = None;
        let mut node = self.root;

        for (i, &direction) in path.iter().enumerate() {
            let mut next_node = node.child(direction).expect("Cannot clone invalid node");
            if next_node.generation != self.generation {
                next_node = next_node.clone();
                next_node.generation = self.generation;
                // Update the parent with the new child node
                if direction == LEFT {
                    node.left = Some(next_node);
                } else {
                    node.right = Some(next_node);
                }
            }
            if i == result_index {
                result = Some(next_node);
            }
            node = next_node;
        }
        // If we complete the loop, return the result
        return result;
    }

    /// Find the leaf node which represents the given offset into the overall data.
    /// Returns (leaf node, offset into the leaf node's data, path taken to get to the leaf node)
    fn leaf_at_index(&self, offset: Offset) -> (RefNode<T>, Offset, Vec<bool>) {
        let mut node = self.root;
        let mut local_offset = offset;
        let mut path = Vec::with_capacity(self.root.rank as usize);
        loop {
            if node.length <= local_offset {
                path.push(RIGHT);
                local_offset -= node.length;
                node = node.right.expect("Cannot index to invalid node");
            }
            match node.left {
                Some(n) => {
                    path.push(LEFT);
                    node = n;
                },
                None => {
                    return (node, local_offset, path);
                },
            }
        }
    }

    /// Private split function which splits the node at the path into two nodes and rebalances
    /// the tree. Takes the path to travel and the local offset to split at.
    /// This function assumes that nodes are safe to modify, path refers to a valid leaf node,
    /// and 0 < local_offset < leaf_node.length.
    fn danger_split(&mut self, path: &[bool], global_offset: Offset, local_offset: Offset) {
        let mut nodes = self.travel_path(path);

        // Grab the leaf node, now with descriptive error messages!
        let leaf = nodes.last().expect("Empty node list, bad parameters");
        // Grab the parent node with a less descriptive error message if the array is too small.
        let mut parent = nodes[nodes.len() - 2];
        // Split the leaf data into the left and right data
        let (data_left, data_right) = (self.on_split)(&leaf.data, local_offset);
        // Starting offset for the leaf
        let start_offset = global_offset - local_offset;
        // Create our new parent node with length = length of left side
        let mut new_parent = RopeNode::new_ref(local_offset, self.generation, self.invalid_data);
        // Create new left and right leaves and correct the ranks and lengths
        new_parent.left = Some(RopeNode::new_ref(local_offset, self.generation, data_left));
        new_parent.right = Some(RopeNode::new_ref(leaf.length - local_offset, self.generation, data_right));
        new_parent.update_rank();
        new_parent.length = local_offset;

        // Replace our current leaf in the parent with the new parent
        if parent.is_left(&leaf) {
            parent.left = Some(new_parent);
        } else {
            parent.right = Some(new_parent);
        }
        // Finally we balance the tree, starting from "parent"
        self.balance_insert(&mut nodes[..nodes.len() - 1]);
    }

    /// Travel path and return the a vector of the located nodes, excluding the root node.
    fn travel_path(&self, path: &[bool]) -> Vec<RefNode<T>> {
        let mut node = self.root;
        let mut nodes = Vec::with_capacity(path.len());
        for &direction in path.iter() {
            let next = if direction == LEFT { node.left } else { node.right };
            node = next.expect("Cannot travel to invalid node");
            nodes.push(node);
        }
        return nodes;
    }

    fn travel_path_leaf(&self, path: &[bool]) -> RefNode<T> {
        let mut node = self.root;
        for direction in path.iter() {
            let next = if direction == LEFT { node.left } else { node.right };
            node = next.expect("Cannot travel to invalid node");
        }
        return node;
    }

    /// Given a path representing a leaf node, modify it to point to the next leaf node.
    /// Returns false if there is no next node.
    fn path_next_leaf(&self, path: &mut Vec<bool>) -> bool {
        self.path_move_leaf(path, RIGHT)
    }

    /// Given a path representing a leaf node, modify it to point to the previous leaf node.
    /// Returns false if there is no previous node.
    fn path_prev_leaf(&self, path: &mut Vec<bool>) -> bool {
        self.path_move_leaf(path, LEFT)
    }

    fn path_move_leaf(&self, path: &mut Vec<bool>, prev_next: bool) -> bool {
        let mut nodes = self.travel_path(path);
        // Go "up" once
        nodes.pop();
        let mut last_lr = path.pop().unwrap();

        // Go "up" until you can go left once
        while last_lr == prev_next || nodes.last().unwrap().child(prev_next).is_none() {
            // Move up if we're at the left or there's no left to move into
            if let Some(lr) = path.pop() {
                last_lr = lr;
                nodes.pop();
            } else {
                // Edge case where we're at the beginning of the tree
                return false;
            }
        }

        // Move left once
        nodes.push(nodes.last().unwrap().child(prev_next).unwrap().clone());
        path.push(prev_next);
        // Move right as far as possible
        while let Some(n_new) = nodes.last().unwrap().child(!prev_next) {
            nodes.push(n_new.clone());
            path.push(!prev_next);
        }

        return true;
    }

    fn balance_insert(&mut self, nodes: &mut [RefNode<T>]) {
        for i in (nodes.len() - 1)..0 {
            let mut node = nodes[i];
            // Update the rank of our node
            node.update_rank();
            if node.is_rank_equal() {
                // The node's two child ranks are equal, meaning our rank didn't change and we
                // can stop here.
                return;
            }
            // Check if we need to rebalance
            let mut parent = nodes.get_mut(i - 1).unwrap_or(&mut self.root);
            if node.need_rotate_left() {
                self.rotate_left(&mut node, parent);
            }
            if node.need_rotate_right() {
                self.rotate_right(&mut node, parent);
            }
        }
        // We reached the root node, we're balanced!
        return;
    }

    fn balance_delete(&mut self, nodes: &mut [RefNode<T>]) {
        unimplemented!()
    }

    /// Rotate a node to the left
    /// 
    /// * `p` - node to rotate
    /// * `pp` - parent of `p`
    fn rotate_left(&mut self, p: &mut RefNode<T>, pp: &mut RefNode<T>) {
        let mut r = p.right.expect("Cannot rotate left if right is None");
        p.right = r.left;
        if pp.is_left(p) {
            pp.left = Some(r);
        } else {
            pp.right = Some(r);
        }
        r.left = Some(*p);
        // Update rope lengths
        r.length += p.length;
        // Update ranks

    }

    /// * `p` - node to rotate
    /// * `pp` - parent of `p`
    fn rotate_right(&mut self, p: &mut RefNode<T>, pp: &mut RefNode<T>) {
        let mut l = p.left.expect("Cannot rotate right if left is None");
        let len_lr = p.length - l.length;
        p.left = l.right;
        if pp.is_left(p) {
            pp.left = Some(l);
        } else {
            pp.right = Some(l);
        }
        l.right = Some(*p);
        // L is no longer in P's left subtree
        p.length -= l.length;
    }
}

impl<T> Clone for Rope<T> {
    fn clone(&self) -> Self {
        let next_gen = (self.generation + 1) % MAX_GENERATION;
        let mut root = self.root.clone();
        root.generation = next_gen;
        // If we're at the max generation, then we need to full-clone the entire tree and reset generations
        Rope {
            root,
            on_split: self.on_split,
            generation: next_gen,
            invalid_data: self.invalid_data
        }
    }
}

pub struct RopeIter<'a, T> {
    rope: &'a Rope<T>,
    start: Offset,
    end: Offset,
    current: Offset,
    path: Vec<bool>,
}

impl<'a, T> Iterator for RopeIter<'a, T> {
    type Item = (T, Offset, Offset);

    fn next(&mut self) -> Option<Self::Item> {
        if self.current >= self.end {
            return None;
        }
        // Grab the current node
        let node = self.rope.travel_path_leaf(&self.path);
        // Update the path
        if !self.rope.path_next_leaf(&mut self.path) {
            return None;
        }
        let new_pos = self.current + node.length;
        let result = (node.data, self.current, new_pos);
        self.current = new_pos;
        return Some(result);
    }
}

#[derive(Clone)]
struct RopeNode<T> {
    /// The number of bytes this node contains
    length: Offset,
    /// The generation of the current node
    generation: u16,
    /// The left child node
    left: Option<RefNode<T>>,
    /// The right child node
    right: Option<RefNode<T>>,
    // The rank of the node
    rank: u8,
    /// The id of the data, this is basically a key to be referenced somewhere else
    data: T,
}

type RefNode<T> = Rc<RopeNode<T>>;

impl<T> RopeNode<T> {
    fn new_ref(length: Offset, generation: u16, data: T) -> RefNode<T> {
        Rc::new(RopeNode {
            length,
            generation,
            left: None,
            right: None,
            rank: 1,
            data,
        })
    }

    fn child(&self, child_name: bool) -> Option<RefNode<T>> {
        if child_name == LEFT { self.left } else { self.right }
    }

    fn is_leaf(&self) -> bool {
        self.left == None && self.right == None
    }

    fn is_left(&self, other: &RopeNode<T>) -> bool {
        match &self.left {
            Some(n) => n.as_ref() == other,
            None => false,
        }
    }

    fn is_right(&self, other: &RopeNode<T>) -> bool {
        match &self.right {
            Some(n) => n.as_ref() == other,
            None => false,
        }
    }

    fn with_generation(&mut self, generation: u16) -> &Self {
        self.generation = generation;
        return self;
    }

    fn update_rank(&mut self) {
        self.rank = cmp::max(RopeNode::rank_of(&self.left), RopeNode::rank_of(&self.right));
    }

    fn is_rank_equal(&self) -> bool {
        RopeNode::rank_of(&self.left) == RopeNode::rank_of(&self.right)
    }

    fn need_rotate_left(&self) -> bool {
        self.rank > RopeNode::rank_of(&self.left) + 2
    }

    fn need_rotate_right(&self) -> bool {
        self.rank > RopeNode::rank_of(&self.right) + 2
    }

    fn rank_of(node: &Option<RefNode<T>>) -> u8 {
        match node {
            Some(n) => n.rank,
            None => 0,
        }
    }

    fn length_of(node: &Option<RefNode<T>>) -> Offset {
        match node {
            Some(n) => n.length,
            None => 0,
        }
    }
}

impl<T: RopeData> PartialEq for RopeNode<T> {
    fn eq(&self, other: &RopeNode<T>) -> bool {
        (self.length == other.length && self.left == other.left && self.right == other.right
                && self.data == other.data && self.generation == other.generation)
    }
}

use std::fs;
use std::io;
use std::collections::BTreeMap;
use super::rope::{Rope, RopeIter};
use platform;

#[cfg(target_os = "windows")]
use std::os::windows::fs::FileExt;
#[cfg(target_os = "linux")]
use std::os::unix::fs::FileExt;

const CACHE_BLOCK: usize = 1024 * 4;
type Result<T> = std::result::Result<T, String>;

pub struct Portion {
    pub offset: u64,
    pub length: u64,
}

impl Portion {
    pub fn end(&self) -> u64 {
        self.offset + self.length
    }
}

struct Node {
    source: usize,
    offset: u64,
}

pub struct EditContext {
    sources: Vec<DataSource>,
    cache: Vec<DataCache>,
    data: Rope<Node>,
}

impl EditContext {
    pub fn new() -> Self {
        Self {
            sources: Vec::new(),
            cache: Vec::new(),
            data: Rope::new(),
        }
    }

    pub fn get_content(&mut self, part: Portion) -> Result<ContentView> {
        let mut iter = self.data.iter(part.offset, part.length);
        ContentView {
            context: self,
            iter,
            area: part,
            current: part.offset,
            local: 0,
            data: /* TODO */,
        }
    }

    pub fn clear_cache(&mut self) {
        unimplemented!()
    }
}

pub struct ContentView<'a> {
    context: &'a EditContext,
    iter: RopeIter<'a, Node>,
    area: Portion,
    current: u64,
    local: usize,
    data: &'a Vec<u8>,
}

impl<'a> io::Read for ContentView<'a> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let mut remaining = buf.len();
        while remaining > 0 && self.current < self.area.end() {
            self.iter.next()
        }
    }
}

pub enum DataSource {
    File(fs::File),
    Disk(platform::DiskView),
    Memory(Vec<u8>),
}

impl DataSource {
    pub fn read(&mut self, offset: u64, length: u64) -> Result<Vec<u8>> {
        let mut data = Vec::new();
        match self {
            DataSource::File(f) => {
                data.resize(length, 0);
                let real_len = if cfg!(windows) {
                    f.seek_read(&mut data, offset)?
                } else if cfg!(unix) {
                    f.read_at(&mut data, offset)?
                } else {
                    use std::io::SeekFrom;
                    f.seek(SeekFrom::Start(offset))?;
                    f.read(&mut data)?
                };
                data.resize(real_len, 0);
            },
            DataSource::Disk(d) => {
                unimplemented!()
            },
            DataSource::Memory(d) => {
                data.extend_from_slice(d[offset..(offset + length)]);
            }
        }
        Ok(data)
    }
}

pub struct DataCache {
    map: BTreeMap<u64, CacheEntry>,
}

struct CacheEntry {
    data: Vec<u8>,
    timestamp: u64,
}
